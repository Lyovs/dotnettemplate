﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebApplication1
{
    public class TestDbMap: IEntityTypeConfiguration<TestDb>
    {
        public void Configure(EntityTypeBuilder<TestDb> builder)
        {
            builder.ToTable("TestDbEmployer");
            builder.HasKey(x => x.EmployeeId);
        }
    }
}
